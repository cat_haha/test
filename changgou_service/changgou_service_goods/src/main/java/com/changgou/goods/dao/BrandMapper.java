package com.changgou.goods.dao;

import com.changgou.goods.pojo.Brand;
import tk.mybatis.mapper.common.Mapper;

/**
 * 品牌信息持久化接口
 */
public interface BrandMapper extends Mapper<Brand> {
}
