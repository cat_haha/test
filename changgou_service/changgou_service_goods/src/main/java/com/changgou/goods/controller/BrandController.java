package com.changgou.goods.controller;

import com.changgou.goods.pojo.Brand;
import com.changgou.goods.service.BrandService;
import com.github.pagehelper.PageInfo;
import entity.Message;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author daisy
 * @Date 2019/12/19 17:57
 */
@RestController
@RequestMapping("brand")
//用于解决js跨域问题-在微服务中，一般都需要此注解
@CrossOrigin
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 查询所有品牌
     * @return
     */
    @GetMapping
    public Result<List<Brand>> findAll(){
        List<Brand> brandList = brandService.findAll();
        return new Result<List<Brand>>(true, StatusCode.OK,"查询品牌信息成功",brandList);
    }

    /**
     * 根据id查询品牌数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Brand> findById(@PathVariable Integer id){
        //根据id查询
        Brand brand = brandService.findById(id);
        return new Result<Brand>(true,StatusCode.OK, "查询品牌成功",brand);
    }

    /**
     * 添加品牌
     * @param brand
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Brand brand){
        brandService.add(brand);
        return new Result(true,StatusCode.OK, "保存品牌成功");
    }

    /**
     * 修改品牌数据
     * @param brand
     * @param id
     * @return
     */
    @PutMapping("{id}")
    public Result update(@RequestBody Brand brand,@PathVariable Integer id){
        //设置id
        brand.setId(id);
        return new Result(true,StatusCode.OK, "修改品牌成功");
    }

    /**
     * 根据id删除品牌数据
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id){
        brandService.delete(id);
        return new Result(true,StatusCode.OK, "删除成功");
    }

    /**
     * 多条件搜索品牌数据
     * @param brand
     * @return
     */
    @PostMapping(value = "search")
    public Result<List<Brand>> findList(@RequestBody(required = false) Brand brand){
        List<Brand> list = brandService.findList(brand);
        return new Result(true,StatusCode.OK, "查询成功",list);
    }

   /* *//**
     * 分页查询
     * @param page
     * @param size
     * @return
     *//*
    @GetMapping(value = "search/{page}/{size}")
    public Result<PageInfo> findPage(@PathVariable int page, @PathVariable int size){
        //分页查询
        PageInfo<Brand> pageInfo = brandService.findPage(page, size);
        return new Result<PageInfo>(true,StatusCode.OK, "查询成功",pageInfo);
    }*/

    @PostMapping(value = "search/{page}/{size}")
    public Result<PageInfo> findPage(@RequestBody(required = false) Brand brand, @PathVariable int page, @PathVariable int size){
        //执行搜索
        PageInfo<Brand> pageInfo = brandService.findPage(brand, page, size);
        return new Result<PageInfo>(true,StatusCode.OK, "查询成功",pageInfo);
    }
}
