package com.changgou.goods.service;

import com.changgou.goods.pojo.Brand;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author daisy
 * @Date 2019/12/19 17:47
 */

public interface BrandService {
    /**
     * 查询所有品牌
     * @return
     */
    List<Brand> findAll();

    /**
     * 根据id查询
     * @param id
     * @return
     */
    Brand findById(Integer id);

    /**
     * 增加
     * @param brand
     */
    void add(Brand brand);

    /**
     * 修改品牌数据
     * @param brand
     */
    void update(Brand brand);

    /**
     * 删除
     * @param id
     */
    void delete(Integer id);

    /**
     * 多条件搜索品牌方法
     * @param brand
     * @return
     */
    List<Brand> findList(Brand brand);

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     *//*
    PageInfo<Brand> findPage(int page, int size);*/

    /**
     * 条件+分页查询
     * @param brand 查询条件
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    PageInfo<Brand> findPage(Brand brand, int page, int size);


}
